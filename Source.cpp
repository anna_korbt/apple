#include <iostream>
#include <vector>
using namespace std;
enum class Color {
    None,
    Red,
    Green,
    Yelow
};

enum class Variety {
    None,
    Golden,
    Gala,
    BlackPrince
};
struct OneAppleStatictic
{
    Color color;
    int min;
    int max;

};

struct GeneralAppleStatictic
{
    int summGreen = 0;
    int summRed = 0;
    int summYelow = 0;
    int sum = 0;
    int maxRed = 0;
    int maxYellow = 0;
    int maxGreen = 0;
    int minRed = 0;
    int minYellow = 0;
    int minGreen = 0;
};

const char* getString(Color color) {
    switch (color)
    {   
    case Color::Red:
        return "red";
        break;
    case Color::Green:
        return "green";
        break;
    case Color::Yelow:
        return "yelow";
        break;
    default:
        return "none";
        break;
    }
}
const char* getStringVariety(Variety variety) {
    switch (variety)
    {
    case Variety::Golden:
        return "golden";
        break;
    case Variety::Gala:
        return "gala";
        break;
    case Variety::BlackPrince:
        return "blackprince";
        break;
    default:
        return "none";
        break;
    }
}
string toLower( const string& str ) {
    //cout << str[0];//�������� ������ �������(������) �� ������
    return str;
}
Color cinColor(const char* text) {
    cout << text;
    string a;
    cin >> a;
    
    if (a == getString(Color::Green)) {
        return Color::Green;
    }
    if (a == getString(Color::Red)) {
        return Color::Red;
    }
    if (a == getString(Color::Yelow)) {
        return Color::Yelow;
    }
    return Color::None;
}

Variety cinVariety(const char* text) {
    cout << text;
    string a;
    cin >> a;
    
    a = toLower(a);

    if (a == getStringVariety(Variety::Golden)) {
        return Variety::Golden;
    }
    if (a == getStringVariety(Variety::Gala)) {
        return Variety::Gala;
    }
    if (a == getStringVariety(Variety::BlackPrince)) {
        return Variety::BlackPrince;
    }
    return Variety::None;
}
class Apple {
public:
    Apple() {
        weight = 0;
        color = Color::None;
        variety = Variety::None;
    }
    int get_weight(){
        return weight;
    }
    void set_weight(int weight) {
        if (weight > 1) {
            this->weight = weight;
        }
        else {
            cout << "Weight is very small"<<endl;
        }
    }

    Variety get_variety() {
        return variety;
    }
    void set_variety(Variety variety) {
        
        this->variety = variety;
    }

    Color get_color() {
        return color;
    }
    void set_color(Color color) {

        this->color = color;
    }

    


    //apple is big
    //vector is array
    //property color located in class Apple
    /*������ � ������ ����
        � ���� ������
        � ��� �����
        ������� ������ � ����
        ���� �������
        ����� �������
        ��� �������� �������
        ��� �����������
        �� ������������� � �����*/
    // i'm listening you now
    //i  listen you
    //i'm going to house
    //Ship is pulling to port
    //blue sky
    //simple class
    //Code is calling function
    //Zoom is Opening
    //he is speking with mother
private:
    int weight;
    Variety variety;
    Color color;
};
void input(vector<Apple>* array_apple) {
    Apple a;
    Color color = Color::None;
    while (true) {
        int weight = 0;
        Variety variety = Variety::None;
        cout << "Massa of apple: ";
        cin >> weight;

        variety = cinVariety("variety of apple : ");

        if (weight == 0) {
            break;
        }

        color = cinColor("color of apple : ");
        a.set_weight(weight);
        a.set_variety(variety);
        a.set_color(color);
        array_apple->push_back(a);
        cout << array_apple->size();

        cout << endl;
    }
}
    void print(GeneralAppleStatictic& general_apple_statictic, vector<Apple> array_apple, OneAppleStatictic& oneApple) {
        cout << "---------------------\n";
        cout << "Green Apple: " << general_apple_statictic.summGreen << endl;
        cout << "Yelow Apple: " << general_apple_statictic.summYelow << endl;
        cout << "Red Apple: " << general_apple_statictic.summRed << endl << endl;

        if (general_apple_statictic.summRed != 0)
        {
            cout << "the heaviest red apple: " << general_apple_statictic.maxRed << endl;
            cout << "the lightest red apple: " << general_apple_statictic.minRed << endl;
        }
        if (general_apple_statictic.summYelow != 0)
        {
            cout << "the heaviest yellow apple: " << general_apple_statictic.maxYellow << endl;
            cout << "the lightest yellow apple: " << general_apple_statictic.minYellow << endl;
        }
        if (general_apple_statictic.summGreen != 0)
        {
            cout << "the heaviest green apple: " << general_apple_statictic.maxGreen << endl;
            cout << "the lightest green apple: " << general_apple_statictic.minGreen << endl;
        }
        cout << endl;

        cout << "total   number of apples: " << array_apple.size() << " pcs" << endl;
        cout << "The heaviest apple: " << oneApple.max << " gram" << endl;
        cout << "The lightest apple: " << oneApple.min << " gram" << endl << endl;

        cout << "Total mass: " << general_apple_statictic .sum << " gram" << endl;
        
        cout << "Avg mass: " << general_apple_statictic.sum / array_apple.size() << " gram " << endl;
    }
    void min_max(vector<Apple>* array_apple, OneAppleStatictic& oneApple, GeneralAppleStatictic& general_apple_statictic) {
        
        for (unsigned int i = 0; i < array_apple->size(); i++) {
            if ((*array_apple)[i].get_weight() > oneApple.max) {
                oneApple.max = (*array_apple)[i].get_weight();
            }

            if ((*array_apple)[i].get_weight() < oneApple.min) {
                oneApple.min = (*array_apple)[i].get_weight();
            }

            general_apple_statictic.sum += (*array_apple)[i].get_weight();

            if ((*array_apple)[i].get_color() == Color::Yelow) {
                general_apple_statictic.summYelow++;
                if ((*array_apple)[i].get_weight() > general_apple_statictic.maxYellow)
                    general_apple_statictic.maxYellow = (*array_apple)[i].get_weight();

                if ((*array_apple)[i].get_weight() < general_apple_statictic.minYellow)
                    general_apple_statictic.minYellow = (*array_apple)[i].get_weight();
            }
            else if ((*array_apple)[i].get_color() == Color::Green) {
                general_apple_statictic.summGreen++;
                if ((*array_apple)[i].get_weight() > general_apple_statictic.maxGreen)
                    general_apple_statictic.maxGreen = (*array_apple)[i].get_weight();

                if ((*array_apple)[i].get_weight() < general_apple_statictic.minGreen)
                    general_apple_statictic.minGreen = (*array_apple)[i].get_weight();
            }
            else if ((*array_apple)[i].get_color() == Color::Red) {
                (general_apple_statictic.summRed)++;
                if ((*array_apple)[i].get_weight() > general_apple_statictic.maxRed)
                    general_apple_statictic.maxRed = (*array_apple)[i].get_weight();

                if ((*array_apple)[i].get_weight() < general_apple_statictic.minRed)
                    general_apple_statictic.minRed = (*array_apple)[i].get_weight();
            }
        }

    }
int main() {
    vector<Apple> array_apple;
    OneAppleStatictic oneApple;
    GeneralAppleStatictic general_apple_statictic;
    input(&array_apple);
    cout<<"aa"<< array_apple.size()<<endl;

    int sum=0;
    if( !array_apple.empty() )//empty - ���������, ������ ������ ��� ���. ������ true - ���� ������
        oneApple.min = general_apple_statictic.minRed = general_apple_statictic.minYellow = general_apple_statictic.minGreen = array_apple[0].get_weight();

    min_max(&array_apple,oneApple, general_apple_statictic);
    print(general_apple_statictic,array_apple, oneApple);


    return 0;
}